#!/bin/bash

# Set working directory to the script location in case the user
# did not run the script in the project root.
cd "${0%/*}"

# Hard reset to origin/main, which should contain the initial state of the lesson
git reset --hard origin/main

# Colors
GREEN="\033[1;32m"
YELLOW="\033[1;33m"
NC="\033[m"

echo -e "$GREEN✔$NC Deleted all your changes successfully!"
echo -e "$YELLOW!$NC Initializing the exercises again..."

# Call init to reinit the exercises
./init.sh