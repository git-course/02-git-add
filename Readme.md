# 📚 Git Learning - Add

This Readme is available in: [English](./Readme.md), [Français](./Readme.fr.md).

In this new course, you will learn how to add files in order to commit them later.

## 🔄 Lifecycle of files in `git`

### 🐺 Tracked files

With git, all your files can have different main statuses:

- Tracked
- Untracked
- Ignored

All theses statuses have different meanings for `git` and will change the behaviour of some commands (such as `git add`).

A tracked file is known by `git`, it has been previously added by someone (maybe you). It means that `git` knows its previous state and can add its modifications when it changes.

An untracked file is not yet known by `git`. It mostly means that this is a new file that has never been added to `git`. You can add it to tracked files.

An ignored file is not known by `git` and should never be. This behaviour happens with files names `.gitignore` that we will see more in depth in their own lesson. They hold names of files, patterns or entire directories that `git` should ignore so that no one can add them to the directory without explicitely telling `git` "hey, add this **ignored** file, I know what I'm doing!".

In this lesson, we will focus on *tracked* and *untracked* files.

### 🪧 Staging contents

When *tracked*, a file also has specific statuses to help differenciate all *tracked* files in the *repository*:

- Unmodified
- Modified
- Staged

*Unmodified* and *Modified* statuses are pretty straightforward. An unmodified file has not been modified since the last time it has been *committed*. A modified file, on the contrary, has been modified and is waiting for its contents to be staged. These statuses are the *working tree*, or *working directory*, they are **not yet** staged.

Staged files are marked as "included in next commit". In other words, you simply say that the changes done to the file (or a part of them) are ready, and you want them to appear in the repository next time you create a *commit*.

This diagram shows the lifecycle graphically (taken from [Git-scm book](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)):

![Lifecycle diagram](./lifecycle.png)

*Note: it is possible to have a file with both the modified and staged statuses, if only some of its changes are staged (we'll learn how below).*

To avoid repeating the same words too much, when we say *"add a file to `git`"*, we mean *"set the status of the file to **tracked** and **stage** its contents"*.


## ☯️ Basic command

The most basic add command will add the entirety of *tracked* and *untracked* specified files and folders to `git` (we mean **track** the files and **stage** its contents, remember?).

When adding a folder, `git` will recursively look at each individual *untracked* or *tracked* file and add it.

This behaviour is simply done with `git add <files>`. Here are a few examples:

```bash
git add Readme.md Readme.fr.md # Add both Readme.md and Readme.fr.md
git add my-awesome-folder/ # Add all files in my-awesome-folder and its subfolders, recursively
git add *.md # Add all files ending with .md
```

In fact, `git` does not really handle `*.md` itself, this is done using [Bash Globbing](https://linuxhint.com/bash_globbing_tutorial/) (available in most modern shells). You may already be familiar with it if you use `bash`, `zsh`, ... often.

## 🔲 Selective add

Some arguments modify the behaviour of `git add`. You are sometimes able to use these arguments and not specify any file or folder, in which case all files in working directory are taken in account. If `<files>` is specified in the command, it means that specifying file(s) or folder(s) is **mandatory**.

| Command                    | New files | Modified files | Deleted files | Comment                              |
| -------------------------- | --------- | -------------- | ------------- | ------------------------------------ |
| `git add -A`               |     ✓     |        ✓       |       ✓       | Stage all files in working directory |
| `git add -u`               |     ✗     |        ✓       |       ✓       | Stage all tracked files              |
| `git add --no-all <files>` |     ✓     |        ✓       |       ✗       | Stage new and modified files         |

In addition to these options, two more arguments can prove useful:

- `git add -p` for **patch mode** (see below).
- `git add -f` to force add a file, useful when it is ignored.

**Patch mode:**

Patch mode is useful when you want to check what you will add, file by file, line by line. If no *pathspec* (one or multiple files - noted `<files>` in previous commands) is specified, only tracked files are taken in account.

For each file, there are multiple *patches* that may be staged. They are shown using a *diff* such as this one:

```diff
-int main(void) {
+int main(int argc, char** argv) {
     return 0;
 }
```

This patch, if accepted, means that the line starting with a `-` symbol will be replaced by the one with a `+` symbol (in our case, `main(void)` will be replaced by `main(int, char**)`).

For each patch, you will have to enter a letter to indicate what you want to do:

- `y` accepts the patch and stages it;
- `n` refuses the patch;
- `q` refuses this patch and all remaining patches, effectively exiting the interactive menu;
- `a` accepts this patch and all remaining patches **in the current file**, and stages them;
- `d` refuses this patch and all remaining patches **in the current file**,
- `g` shows a new interface to choose a patch to jump to;
- `s` splits the current patch in multiple smaller patches, if possible;
- `e` opens your preferred editor for manual patch edition, in which you'll be able to change the `+`, `-` and `#` (comment) symbols at the beginning of each line, effectively modifying the staged patch;
- `?` shows a message explaining what all options do (similar to this list).

*Note: an empty folder cannot be added to git, since adding a folder implies adding each file individually. Developers tend to add a `.keep` empty file in order to add an empty folder.*

## 🟢 Status

It is often useful to know the status of your files: which ones are tracked, which ones aren't, which are in your working directory and which ones are staged.

You may do so with `git status`.

For this lesson, for example, my current `git status` is

```git
On branch main

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
	new file:   Readme.md

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   Readme.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	lifecycle.png
```

The first line tells me that I'm working on branch `main` (which I know is my default branch). The second line tells me that I've made no commits yet (I will when I'm finished writing this file, and the line will disappear).

**Changes to be committed** corresponds to the staged files, **Changes not staged for commit** to the tracked files in the working directory, and **Untracked files** to my new files which are not tracked yet.

This command is really verbose, and you can use `git status -s` for a short overview of file statuses:

```git
AM Readme.md
?? lifecycle.png
```

Each tracked file in the working directory, staged or new file will appear, with one to two symbols in front of its name. The leftmost character indicates the *staging status*, and the rightmost character indicates the working directory status:

- `A` indicates that the file has been added (a new file has been staged).
- `M` indicates that the file has been modified.
- `??` indicates that the file is new and has not yet been staged (untracked).

In our example, `Readme.md` is a new file that has been staged (`A` in the left column) and modified since (`M` in the right column, indicating modifications in working directory), and `lifecycle.png` is untracked.

## 🧑‍🎓 Exercises

This lesson contains two exercises. You may initialize them by executing the `init.sh` script. If your commands break the repo, you may restore its initial state with `reset.sh`.

*Be careful: you must not **edit** the files yourself, you only have to use the `git` tool to do so. Failure to comply may break the verification script and you will be expelled from our notorious **Git School***

### Exercise 1

Your first exercise is pretty simple. A new file, `adele`, has been created, and file `panda` has been modified. You must add them entirely to staged files, but you **must not** do the same with `random`.

### Exercise 2

This exercise is a bit harder, but everything is explained in this lesson. You have to stage some of the modifications applied to `random`. Two lines have been added, only one of them must be staged.