#!/bin/bash

# Set working directory to the script location in case the user
# did not run the script in the project root.
cd "${0%/*}"

# Retrieve first commit in history
FIRST_COMMIT="$(git rev-list --max-parents=0 HEAD)"

# Correct 
EXPECTED_COMMIT='68e52ffd8541d78157f99cf3b98972e531f554ed'

# Colors
GREEN="\033[1;32m"
RED="\033[0;31m"
NC="\033[m"

if [ "$FIRST_COMMIT" != "$EXPECTED_COMMIT" ]; 
then
    echo -e "$RED✘$NC"" Repository verification failed. Have you used the git clone command?"
    exit 1
fi;

## Exercise 1

# Create new file
echo "Hello, it's me" > adele

# Modify panda file
echo "Yes, I am!" >> panda

## Exercice 2
cat <<< "Lorem ipsum dolor sit amet, consectetur adipiscing elit.

$(cat random)

Aliquam maximus eu mi ac consequat." > random

echo -e "$GREEN✔$NC Exercises initialization complete. Good luck! :)"