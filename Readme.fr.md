# 📚 Git Learning - Add

This Readme is available in: [English](./Readme.md), [Français](./Readme.fr.md).

Dans ce nouveau cours, tu apprendras comment ajouter des fichiers pour ensuite faire un commit.

## 🔄 Le cycle de vie des fichiers dans `git`

### 🐺 Fichiers tracés (*tracked* en anglais)

Avec git, tous tes fichiers ont un statut principal, parmi :

- Tracked (tracé)
- Untracked (non tracé)
- Ignoré

Tous ces statuts ont un sens différent pour `git` et modifient le comportement de certaines commandes (notamment `git add`).

Un fichier *tracked* est connu par `git`. Il a été ajouté précédemment par quelqu'un (peut-être toi). `git` connaît donc son ancien état (son ancien contenu) and peut ajouter ses modifications lorsque tu l'édites.

Un fichier *untracked* n'est pas encore connu par `git`. Il s'agit simplement d'un nouveau fichier qui n'a encore jamais été ajouté à `git`. Tu peux l'ajouter aux fichiers *tracked*, ce que nous allons voir dans la prochaine section.

Un fichier ignoré n'est pas connu par `git` et ne devrait jamais l'être. Ce comportement est possible grâce à des fichiers nommés `.gitignore` dont nous parlerons en détails dans un autre cours. Ces fichiers contiennent des noms de fichiers, de dossiers et des patterns pour spécifier quels fichiers ne doivent **jamais** être connus. Le seul moyen de les ajouter aux fichiers *tracked* est donc de dire explicitement "hey `git`, j'ai ce fichier que tu **ignores**, et je veux que tu l'ajoutes, je sais ce que je fais !".

Dans ce cours, nous allons nous concentrer sur les fichiers *tracked* et *untracked*.

### 🪧 La zone de staging

Quand des fichiers sont *tracked*, ils ont un autre statut particulier pour les différencier au sein du repo :

- Non modifié (unmodified)
- Modifié (modified)
- Staged

Les statuts *non modifié* et *modifiés* sont assez simples. Un fichier non modifié n'a pas été modifié depuis le dernier *commit*. Un fichier modifié a quant à lui été modifié et attend que son contenu soit ajouté à la zone de staging. Ces deux status sont connus sous le nom de *working tree* (arbre de travail) ou *working directory* (répertoire de travail). Ils ne sont **pas encore** dans la zone de staging (*stage area*).

Les fichiers dans la *stage area* sont marqués comme "inclus dans le prochain commit". Dit autrement, tu peux simplement imaginer que les changements effectués au fichiers (ou une partie d'entre eux) sont prêts, et que tu veux qu'ils apparaissent dans le repo la prochaine fois que tu fais un *commit*.

Ce diagramme montre le cycle graphiquement (pris sur [Git-scm book](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)):

![Lifecycle diagram](./lifecycle.png)

*Note : il est possible d'avoir un fichier à la fois modifié et dans la stage area, si seulement une partie de son contenu a été ajouté (on va apprendre comment).*

Pour éviter des répétitions, lorsque l'on parle "d'ajouter un fichier à `git`", on veut dire "ajoute le fichier aux fichiers *tracked* et met son contenu dans la *stage area*". 


## ☯️ Un `git add` basique

Le `git add` le plus basique va ajouter tous les fichiers et dossiers spécifiés (sauf ceux ignorés) à `git` (on veut dire qu'on les marque comme *tracked* et qu'on stage leur contenu, tu t'en souviens ?).

Quand on ajoute un dossier, `git` regarde chaque fichier individuellement de manière récursive, et l'ajoute s'il n'est pas ignoré.

Ce comportement est possible avec un simple `git add <fichiers>`. Voici quelques exemples :

```bash
git add Readme.md Readme.fr.md # Ajoute à la fois Readme.md et Readme.fr.md
git add mon-super-dossier/ # Ajoute tous les fichiers des dossiers mon-super-dossier et ses sous-dossiers, récursivement
git add *.md # Ajoute tous les fichirs avec comme extension .md
```

En réalité, `git` not gère pas lui-même l'argument `*.md`. C'est [le Globbing Bash](https://linuxhint.com/bash_globbing_tutorial/) (ressource anglaise) qui s'en charge. Cette fonctionnalité est disponible dans la plupart des shells modernes, tu la connais peut-être déjà si tu utilises `bash`, `zsh`, etc. souvent.

## 🔲 `git add` sélectif

Certains arguments modifient le comportement de `git add`. Tu peux parfois utiliser ces arguments sans spécifier de fichier ou dossier. Dans ce cas, tous les fichiers du répertoire de travail (*working directory*) sont pris en compte. Si la commande spécifie explicitement `<files>`, tu n'as pas le choix, il faut **absolument** spécifier des fichiers, au risque sinon de voir la commande échouer en silence.

| Commande                   | Nouveaux fichiers | Fichiers modifiés | Fichiers supprimés | Commentaire                                    |
| -------------------------- | ----------------- | ----------------- | ------------------ | ---------------------------------------------- |
| `git add -A`               |         ✓         |         ✓         |          ✓         | Stage tous les fichiers du *working directory* |
| `git add -u`               |         ✗         |         ✓         |          ✓         | Stage tous les fichiers *tracked*              |
| `git add --no-all <files>` |         ✓         |         ✓         |          ✗         | Stage les fichiers nouveaux ou modifiés        |

En plus de ces options, deux autres arguments peuvent être utiles :

- `git add -p`, pour le **mode patch**, décrit en dessous.
- `git add -f`, pour forcer l'ajoute d'un fichier ignoré. 

**Mode patch :**

Le mode patch est utile quand tu veux vérifier ce que tu vas ajouter, fichier après fichier, ligne après ligne. Si tu ne spécifies aucun fichier ou dossier, seul les fichiers *tracked* sont pris en compte.

Pour chaque fichier, il y a plusieurs *patches* qui vont t'être présentés pour être ajouté à la stage area ou non. Ils utilisent le format d'un *diff* comme celui-ci :

```diff
-int main(void) {
+int main(int argc, char** argv) {
     return 0;
 }
```

Ce patch, si accepté, signifie que la ligne commençant par un `-` va être remplacée par celle avec un `+` (elle est supprimée et l'autre est ajoutée). Dans notre cas, la fonction `main(void)` est remplacée par `main(int, char**)`.

Pour chaque patch, tu vas entrer une lettre qui indique ce que tu souhaites faire :

- `y` accepte le patch et l'ajoute à la stage area ;
- `n` refuse le patch ;
- `q` refuse ce patch et tous ceux qui suivent, arrêtant l'exécution de la commande ;
- `a` accepte ce patch et tous ceux qui suivent **dans le même fichier**, et les ajoute à la stage area ;
- `d` refuse ce patch et tous ceux qui suivent **dans le même fichier**,
- `g` montre une interface pour choisir à quel patch sauter ;
- `s` essaie de couper le patch actuel en plusieurs petits patches, si possible ;
- `e` ouvre ton éditeur préféré dans lequel tu pourras éditer les `+`, `-` et `#` (commentaire) au début de chaque ligne, pour modifier le patch qui sera appliqué et ajouté à la stage area ;
- `?` montre un message expliquant les différentes options, similaire à cette liste.

*Note : un dossier vide ne peut pas être ajouté à git, puisque l'ajout d'un dossier implique d'ajouter chaque fichier individuellement. Pour contourner cette limitation, il est usuel de créer un fichier `.keep` dans le dossier pour pouvoir l'ajouter.*

## 🟢 Status

Il est souvent utile de connaître le statut de nos fichiers : lesquels sont *tracked*, lesquels ne le sont pas, lesquels sont dans le répertoire de travail et lesquels sont dans la stage area. La commande `git status` nous donne ces informations.

Pour ce cours par exemple, mon `git status` actuel est (en anglais) :

```git
On branch main

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
	new file:   Readme.md

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   Readme.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	lifecycle.png
```

La première ligne m'explique que je travaille sur la branche `main` (dont je sais qu'elle est ma branche par défaut). La seconde ligne m'indique que je n'ai pas encore fait de commit (cette ligne disparaîtra lorsque j'en ferai un).

**Changes to be committed** correspond aux fichiers dans la stage area, **Changes not staged for commit** à ceux qui sont *tracked* mais sont uniquement dans le répertoire de travail, et **Untracked files** à ceux qui ne sont pas encore *tracked*.

Cette commande est très verbeuse, et `git status -s` permet d'afficher les mêmes informations de manière plus concise :

```git
AM Readme.md
?? lifecycle.png
```

Chaque fichier *tracked* dans le répertoire de travail, dans la stage area ou tout nouveau fichier va apparaître, avec deux lettres au début. La première lettre indique le statut dans la *staging area*, et celui de droite dans le répertoire de travail.

- `A` indique que le fichier a été ajouté (un nouveau fichier a été staged).
- `M` indique que le fichier a été modifié.
- `??` indique que le fichier est nouveau et n'est pas encore *tracked*.

Dans notre exemple `Readme.md` est un nouveau fichier qui a été ajouté à la *stage area* (indiqué par le `A` à gauche) et modifié depuis (indiqué par le `M` à droite, qui indique des modifications dans le répertoire de travail), et `lifecycle.png` n'est pas *tracked*.

## 🧑‍🎓 Exercices

Ce cours contient deux exercices. Pour commencer, tu dois exécuter le script `init.sh`. Tu peux revenir à l'état de base en cas de problème avec `reset.sh`.

*Attention : tu ne dois pas **éditer** les fichiers toi-même, mais seulement les manipuler à l'aide de `git`. En cas de non-respect de cette règle, tu pourrais casser le script de vérification et perdre ta place dans la prestigieuse **École de Git**.*

### Exercice 1

Ton premier exercice est assez simple. Un nouveau fichier nommé `adele` a été créé, et le fichier `panda` a été modifié. Tu dois les ajouter entièrement à la stage area, mais tu **ne dois pas** ajouter `random`.

### Exercice 2

Cet exercice est un peu plus compliqué, mais tout est expliqué dans ce cours. Tu dois ajouter à la *stage area* quelques modifications appliquées à `random`. Nous avons automatiquement ajouté deux lignes au fichier, tu dois réussir à en ajouter une seule.