#!/bin/bash

# Set working directory to the script location in case the user
# did not run the script in the project root.
cd "${0%/*}"

# Colors
GREEN="\033[1;32m"
RED="\033[0;31m"
NC="\033[0m"

# Expected output
# A  adele
# M  panda
# MM random

# Check exercise 1
git status -s | grep 'A  adele' > /dev/null
adele=$? # If found, adele=0, if not, adele=1

git status -s | grep 'M  panda' > /dev/null
panda=$? # Same here

if (( adele==0 && panda==0 ));
then 
    echo -e "$GREEN✔$NC"" Exercise 1 correctly done! Congratz!";
else
    echo -e "$RED✘$NC"" Exercise 1 failed. Try again ;)";
fi;

# Check exercise 2
git status -s | grep 'MM random' > /dev/null
random=$?

if (( random==0 ));
then
    echo -e "$GREEN✔$NC"" Exercise 2 seems good. I am impressed!";
else
    echo -e "$RED✘$NC"" Exercise 2 failed. Don't give up yet!"
fi;